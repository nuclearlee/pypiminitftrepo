import numpy as np
import busio
import board
import adafruit_lsm303_accel
import adafruit_lsm303agr_mag




class Acceleration():
    def __init__(self, x=0, y=0, z=0, timeInterval=0):
        # default l/t^2, if you change from default
        # (x,y,z) and timeInterval units will change accordingly
        self.accerlationUnits = "m/s^2"
        self.x = x
        self.y = y
        self.z = z
        self.timeInterval = timeInterval

class CurrentAcceleration():
    pass

class AverageAcceleration(Acceleration):
    # default time step to average over is 0.1s
    def __init__(self, x=0, y=0, z=0, timeInterval=0, timeStepForAverage=1.0):
        self.timeStepForAverage = timeStepForAverage
        self.currentTimeStepTotal = 0.0
        self.__averageAcceleration = 0.0
        Acceleration.__init__(self, x, y, z, timeInterval)

    def ResetTimeStepForAverage(self):
        self.currentTimeStepTotal = 0.0
        
    def setAccelerationVector(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
        
    def GetAverageAcceleration(self, timeStep):
        magnitude = (self.x**2 + self.y**2 + self.z**2)**0.5
        prevTimeTotal = self.currentTimeStepTotal 
        self.currentTimeStepTotal += timeStep
        self.__averageAcceleration = (self.__averageAcceleration * prevTimeTotal + magnitude * timeStep) / self.currentTimeStepTotal
        if self.currentTimeStepTotal > self.timeStepForAverage:
            self.ResetTimeStepForAverage()
        return self.__averageAcceleration
        

class Accelerometer():
    def __init__(self):
        self.isInitialized = False
        # todo, calibrate with input from
        # a minipitft button push
        self.isCalibrated = False
        self.accel = None
    
    def SetupAccelerometer(self):
        i2c = busio.I2C(board.SCL, board.SDA)
        self.accel = adafruit_lsm303_accel.LSM303_Accel(i2c)
        self.isInitialized = True

