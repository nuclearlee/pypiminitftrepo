from PIL import Image, ImageDraw, ImageFont
import SetupMiniPiTFT as setupDisplay
import subprocess
import time
PADDING = -2
X_INIT = 0
Y_INIT = 0

class ImageData():
    def __init__(self):
        self.disp = setupDisplay.SetupDisplay()
        self.height = self.disp.width
        self.width = self.disp.height
        self.image = Image.new('RGB', (self.width, self.height))
        self.draw = ImageDraw.Draw(self.image)
        self.rotation = 0
        # pad the top and bottom
        self.paddedTop = PADDING
        self.paddedBottom = self.height - PADDING
        self.font = ImageFont.truetype('/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf', 20)
        self.currentX = X_INIT
        self.currentY = Y_INIT
        self.currentLineNumber = 0

    # even if you're not drawing the cleared screen, you need to clear it
    # so your text doesn't get garbled'
    def ClearScreen(self):
        self.currentLineNumber = 0
        self.currentX = X_INIT
        self.currentY = Y_INIT
        self.draw.rectangle((0, 0, self.width, self.height), outline=0, fill=0)
        
    def ClearTopHalfOfScreen(self):
        halfHeight = int(self.height / 2)
        self.draw.rectangle((0, 0, self.width, halfHeight), outline=0, fill=0)


    # if you are printing text and connect to a pi zero
    # this will position the text pointing down to the
    # power and usb connector
    def RotateScreen90Degrees(self):
        rotation = 90
        self.RotateScreen(rotation)

    def RotateScreen180Degrees(self):
        rotation = 180
        self.RotateScreen(rotation)

    # if you are printing text and connect to a pi zero
    # this will position the text pointing down to the
    # gpio
    def RotateScreen270Degrees(self):
        rotation = 270
        self.RotateScreen(rotation)

    def RotateScreen(self, rotation):
        self.rotation = rotation
        self.disp.image(self.image, self.rotation)
        
##     def checkLineHeights():
##         pass
##

    def SetLineNumber(self, lineNumber):
        self.currentLineNumber = lineNumber

    def ZeroLineNumber(self):
        self.currentLineNumber = 0
        
    def DrawNewLine(self, text, color):
        self.currentLineNumber += 1
        self.draw.text((self.currentX,self.currentY), text, font=self.font, fill=color)
        widthl,heightl = self.draw.textsize(text, self.font)
        self.currentY += heightl + 1

    def DrawTest(self):
        y = self.paddedTop
        line1 = "LINE1"
        y += self.font.getsize(line1)[1]
        self.draw.text((self.currentX, y), line1, font=self.font, fill="#FFFFFF")
        line2 = "LINE2"
        y += self.font.getsize(line2)[1]
        self.draw.text((self.currentX, y), line2, font=self.font, fill="#FFFF00")
        line3 = "LINE3"
        y += self.font.getsize(line3)[1]
        self.draw.text((self.currentX, y), line3, font=self.font, fill="#00FF00")
        line4 = "LINE4"
        y += self.font.getsize(line4)[1]
        self.draw.text((self.currentX, y), line4, font=self.font, fill="#0000FF")
        line5 = "LINE5"
        y += self.font.getsize(line5)[1]
        self.draw.text((self.currentX, y), line5, font=self.font, fill="#FF00FF")
        line6 = "LINE6"
        y += self.font.getsize(line6)[1]
        self.draw.text((self.currentX, y), line6, font=self.font, fill="#FFFFFF")
        
    def DrawSystemStats(self):
        print("startstat: " + str(int(time.time())))
        # Shell scripts for system monitoring from here:
        # https://unix.stackexchange.com/questions/119126/command-to-display-memory-usage-disk-usage-and-cpu-load
        cmd = "hostname -I | cut -d\' \' -f1"
        IP = "iP: " + subprocess.check_output(cmd, shell=True).decode("utf-8")
        cmd = "top -bn1 | grep load | awk '{printf \"cPU Load: %.2f\", $(NF-2)}'"
        CPU = subprocess.check_output(cmd, shell=True).decode("utf-8")
        cmd = "free -m | awk 'NR==2{printf \"Mem: %s/%s MB  %.2f%%\", $3,$2,$3*100/$2 }'"
        MemUsage = subprocess.check_output(cmd, shell=True).decode("utf-8")
        cmd = "df -h | awk '$NF==\"/\"{printf \"Disk: %d/%d GB  %s\", $3,$2,$5}'"
        Disk = subprocess.check_output(cmd, shell=True).decode("utf-8")
        cmd = "cat /sys/class/thermal/thermal_zone0/temp |  awk \'{printf \"CPU Temp: %.1f C\", $(NF-0) / 1000}\'" # pylint: disable=line-too-long
        Temp = subprocess.check_output(cmd, shell=True).decode("utf-8")

        # Write four lines of text.
        y = self.paddedTop
        self.draw.text((self.currentX, y), IP, font=self.font, fill="#FFFFFF")
        y += self.font.getsize(IP)[1]
        self.draw.text((self.currentX, y), CPU, font=self.font, fill="#FFFF00")
        y += self.font.getsize(CPU)[1]
        self.draw.text((self.currentX, y), MemUsage, font=self.font, fill="#00FF00")
        y += self.font.getsize(MemUsage)[1]
        self.draw.text((self.currentX, y), Disk, font=self.font, fill="#0000FF")
        y += self.font.getsize(Disk)[1]
        self.draw.text((self.currentX, y), Temp, font=self.font, fill="#FF00FF")
        print("endstat: " + str(int(time.time())))
