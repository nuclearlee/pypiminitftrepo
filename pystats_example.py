import time
from MiniPiTFTDrawing import ImageData
from SetupMiniPiTFT import EnableBacklight
     

# setup the display and image
imageData = ImageData()

imageData.ClearScreen()
# rotate the screen
imageData.disp.image(imageData.image, imageData.rotation)

EnableBacklight()


while True:
    imageData.ClearScreen()
    
    imageData.DrawSystemStats()
    # update and rotate image.
    print("startstatdraw: " + str(int(time.time())))
    imageData.disp.image(imageData.image, imageData.rotation)
    print("endstatdraw: " + str(int(time.time())))
    time.sleep(0.1)
