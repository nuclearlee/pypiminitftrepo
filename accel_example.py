import time
from MiniPiTFTDrawing import ImageData
from SetupMiniPiTFT import EnableBacklight
import busio
import board
import adafruit_lsm303_accel
import adafruit_lsm303agr_mag

# setup the display and image
imageData = ImageData()

imageData.ClearScreen()

EnableBacklight()


i2c = busio.I2C(board.SCL, board.SDA)
#mag = adafruit_lsm303agr_mag.LSM303AGR_Mag(i2c)
accel = adafruit_lsm303_accel.LSM303_Accel(i2c)

imageData.RotateScreen270Degrees()

while True:
    imageData.ClearScreen()

    accel_header = "(x,y,z) m/s^2"
    color = (255,0,0)
    imageData.DrawNewLine(accel_header, color)

    acceleration = accel.acceleration
    accelstr = "(%0.1f,%0.1f,%0.1f)"%acceleration
    color = (0,0,255)
    imageData.DrawNewLine(accelstr, color)
    
    accel_magnitude_header = "total accel (m/s^2)"
    color = (0,255,200)
    imageData.DrawNewLine(accel_magnitude_header, color)

    acceleration_magnitude = (acceleration[0]**2 + acceleration[1]**2 + acceleration[2]**2)**0.5 - 10.0
    imageData.DrawNewLine("%0.1f"%acceleration_magnitude, color)
    
###    mag_head = "(x,y,z) T"
###    widthl,heightl = imageData.draw.textsize(accelstr, imageData.font)
###    new_y += heightl + 1
###    imageData.draw.text((0,new_y), mag_head, font=imageData.font, fill=(0, 255, 0))
###
###    magstr = "(%0.1f,%0.1f,%0.1f)"%mag.magnetic
###    widthl,heightl = imageData.draw.textsize(mag_head, imageData.font)
###    new_y += heightl + 1
###    imageData.draw.text((0,new_y), magstr, font=imageData.font, fill=(0, 0, 255))

    imageData.disp.image(imageData.image,imageData.rotation)
    
#    time.sleep(0.1)
