import time
import sys
import os
from MiniPiTFTDrawing import ImageData
from SetupMiniPiTFT import EnableBacklight, DisableBacklight
from acceleration import Accelerometer, AverageAcceleration


def main():
    # setup the display and image
    imageData = ImageData()
    # setup the accelerometer
    instrumentAccel = Accelerometer()
    instrumentAccel.SetupAccelerometer()

    
    imageData.RotateScreen270Degrees()
    imageData.ClearScreen()

    EnableBacklight()


    accelerator = AverageAcceleration()
    print("accel from accel: " + str(accelerator.x))

    while True:
        imageData.ClearScreen()

        accel_header = "(x,y,z) m/s^2"
        color = (255,0,0)
        imageData.DrawNewLine(accel_header, color)

        startTimer = time.time()
        acceleration = instrumentAccel.accel.acceleration
        timeStep = time.time() - startTimer
        accelstr = "(%0.1f,%0.1f,%0.1f)"%acceleration
        color = (0,0,255)
        imageData.DrawNewLine(accelstr, color)
        
        accel_magnitude_header = "accel mag (m/s^2)"
        color = (0,255,200)
        imageData.DrawNewLine(accel_magnitude_header, color)
        imageData.DrawNewLine("%0.2f"%((acceleration[0]**2+acceleration[1]**2+acceleration[2]**2)**0.5), color)
        
        accelerator.setAccelerationVector(acceleration[0], acceleration[1], acceleration[2])
        average_acceleration = accelerator.GetAverageAcceleration(timeStep)
        color = (120,120,120)
        imageData.DrawNewLine("avg accel (m/s^2)", color)
        imageData.DrawNewLine("%0.2f"%average_acceleration, color)
    
        imageData.disp.image(imageData.image,imageData.rotation)
        time.sleep(0.01)

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        DisableBacklight()
        try:
            print("keyboard interrupt, exiting")
            sys.exit(1)
        except:
            os._exit(1)

    
